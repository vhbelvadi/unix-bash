#!/bin/bash
 
#WORK TO BE DONE:
#
#------- 1 -------
#Works only as if for one leapyear or consecutive non-leapyears.
#The problem most likely lies in line #145 where the IF branch $while_counter=$nextly is broken. 
#
#------- 2 -------
#Day counts when no month is specified is still rusty, but that should not matter too much given
#that the core purpose of this programme is to calculate years, months and days when the initial
#month is given.
#
#
 
#----------------------------------------------- START ------------------------------------------#
 
#------- Part 1 - Initialisations ---------------------------------------------------------------#
 
#Initialise the tryagain option to Yes (Y) to start the programme
tryagain=0
 
#Check the tryagain WHILE  loop
while [ $tryagain != 'N' ] &&  [ $tryagain != 'n' ];
do
 
#Let's clear the screen before we forget it
tput clear
 
#Declare a skeleton array for the lengths of months
a=(31 28 31 30 31 30 31 31 30 31 30 31)
 
#Let's initialise everything we'll ultimately calculate, like like good, decent coders should!
months=0
years=0
x=0
 
#Initialise a while_counter to count number of times the while loop runs in Part 3
#i.e. to count  years
while_counter=0
 
#Nextly counter to check when the next leapyear will occur after the specified initial year
nextly=1
 
#------- Initialised ----------------------------------------------------------------------------#
#------- Part 2 - LOGIC to set months-length array a --------------------------------------------#
 
echo "Enter the number of days:"
read x
echo "Would you like to pick the initial month (Y/N)?"
read ch
 
#No initial month condition
if [ $ch = 'N' ] || [ $ch = 'n' ] ;
then m=0
fi
 
#YES initial month condition
if [ $ch = 'Y' ] || [ $ch = 'y' ];
then echo "Enter the initial month (MM):"
read m
m=`expr $m - 1`
fi
 
echo "Would you like to specify the year? (Y/N)?"
read ch
 
#YES initial year condition
if [ $ch = 'Y' ] || [ $ch = 'y' ];
then
echo "What year was it (YYYY)?"
read y
 
#Check automatically if y is a leapyear
y4m=$(echo "$y / 1000"  | bc)
y3n=$(echo "$y - ( $y4m * 1000 )" | bc)
y3m=$(echo "$y3n / 100" | bc)
y2n=$(echo "$y3n - ( $y3m * 100 )" | bc)
j=`expr $y2n % 4`
if [ $j = 0 ];
then a[1]=29
nextly=4
else a[1]=28
 
#To check when the next leapyear, nextly,  will occur
y2n1=`expr $y2n + 1`
j=`expr $y2n1 % 4`
while [ $j != 0 ];
do
y2n1=`expr $y2n1 + 1`
nextly=`expr $nextly + 1`
j=`expr $y2n1 % 4`
done
fi
 
#NO initial year condition
elif [ $ch = 'N' ] || [ $ch = 'n' ];
then echo "Do you know if it is a leapyear (Y/N)?"
read ch
 
#NO initial leapyear condition
if [ $ch = 'N' ] || [ $ch = 'n' ];
then a[1]=28
 
#YES initial leapyear condidion
elif [ $ch = 'Y' ] || [ $ch = 'y' ];
then echo "Was it a leapyear (Y/N)?"
read ch
if [ $ch = 'Y' ] || [ $ch = 'y' ];
then a[1]=29
nextly=4
elif [ $ch = 'N' ] || [ $ch = 'n' ];
then a[1]=28
fi
fi
fi
 
#------- Array prepared -------------------------------------------------------------------------#
#------- Part 3 - LOGIC to count number of whole months in given number of days -----------------#
 
#First FOR loop from specified month m to end of that year
for (( i=m;  i<=11; i=i+1 ))
do
if [ $x -ge ${a[$i]} ];
then
x=`expr $x - ${a[$i]}`
months=`expr $months + 1`
fi
done
 
#Reset counter i to zero in expectation of running the next FOR loop
i=-1
 
#Check if current or next year is a leapyear and reset array accordingly
if [ $nextly = 4 ] || [ $nextly = 3 ] || [ $nextly = 2 ];
then a=(31 28 31 30 31 30 31 31 30 31 30 31)
elif [ $nextly = 1 ];
then a=(31 29 31 30 31 30 31 31 30 31 30 31)
fi
 
#Second FOR loop for if more than one month's worth of days are remaining in x
while [ $x -gt ${a[$(i=`expr $i + 1`)]} ];
do
 
#Check if current year is a leapyear
if [ $while_counter = $nextly ];
then a=(31 29 31 30 31 30 31 31 30 31 30 31)
else a=(31 28 31 30 31 30 31 31 30 31 30 31)
fi
for (( i=0; i<=11; i=i+1 ))
do
if [ $x -ge ${a[$i]} ];
then
x=`expr $x - ${a[$i]}`
months=`expr $months + 1`
fi
done
 
while_counter=`expr $while_counter + 1`
i=-1
done
 
#Uncomment the next line only if the programme counts one day short
#x=`expr $x + 1`
 
#------- Number of months and leftover days obtained --------------------------------------------#
#------- Part 4 - LOGIC to convert number of months into years if they exceed 11 ----------------#
 
while [ $months -ge 12 ];
do
years=`expr $months / 12`
months=$(echo "$months - (12 * $years)" | bc)
done
 
#------- Number of years, months and days now fully obtained ------------------------------------#
#------- Part 5 - Print the final answer --------------------------------------------------------#
 
#If there are no years or months or days
if [ $years = 0 ] && [ $months = 0 ] && [ $x = 0 ];
then echo "Nice joke! Zero days is zero years, zero months and zero days. How's that?"
 
#If there are no years or months
elif [ $years = 0 ] && [ $months = 0 ] && [ $x -ne 0 ];
then echo "Huh! That's the usual $x days. It's not even a month, let alone a year!"
 
#If there are no years or days
elif [ $years = 0 ] && [ $x = 0 ] && [ $months -ne 0 ];
then echo "That's $months months! Would you like to try again (Y/N)?"
read tryagain
 
#If there are no months or days
elif [ $months = 0 ] && [ $x = 0 ] && [ $years -ne 0];
then echo "That's $years years! Would you like to try again (Y/N)?"
read tryagain
 
#If there are no years
elif [ $years = 0 ] && [ $months -ne 0 ] && [ $x -ne 0 ];
then echo "That's $months months and $x days. Would you like to try again (Y/N)?"
read tryagain
 
#If there are no days
elif [ $x = 0 ] && [ $months -ne 0 ] && [ $years -ne 0 ];
then echo "That's $years years and $months months. Would you like to try again (Y/N)?"
read tryagain
 
#If there are no months
elif [ $months = 0 ] && [ $years -ne 0 ] && [ $x -ne 0 ];
then echo "That's $years years and $x days. Would you like to try again (Y/N)?"
read tryagain
 
#If there are years, month and days
else echo "That's $years years, $months months and $x days. Would you like to try again (Y/N)?"
read tryagain
fi
 
#END the tryagain while loop
done
 
#You do want the programme to end, don't you?
exit 0
 
#------- THE END --------------------------------------------------------------------------------#