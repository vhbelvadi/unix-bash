﻿ Welcome to V.H. Belvadi's UNIX-Bash repository
------------------------------------------------
This is a public repository for obvious reasons.
The aim of this repository is to serve as a ref-
erence/knowledge bank for anybody interested in
UNIX, specifically the SH and/or BASH shells.

The scripts in this repository are either comp-
lete or in an acceptably working condition. The
scripts have been written with clear explanation
regarding their logic provided as comments where
required.

These scripts are beginner to intermediate level
and deal with both common or uncanny problems.

You may fork this if you see the need, or dis-
tribute it or simply copy it and work on it by
yourself. If you plan to share or use this pub-
licly, all that is requested of you is to provide
a courtesy link to http://vhbelvadi.com if you
feel it appropriate.

Regards,
VHB
-------------------------------------------------